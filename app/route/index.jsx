import {useBoolState, useInterval, useLatest, useSetState} from '@havlasme/react-toolkit'
import cc from 'classcat'
import {addSeconds, format, isEqual, set, setMilliseconds, setSeconds, subSeconds} from 'date-fns'
import {useCallback, useState} from 'react'
import {Backdrop, Button, Heading, Main, MDI, Panel, TextField, View} from '../ui'

/**
 * The `/` route.
 *
 * @return {JSX.Element}
 * @component
 * @private
 */
const ROUTE__index = function () {
  // the option object.
  const [option, setOption] = useSetState(
    function () {
      return {
        framerate: 25,
        interval: 100,
        location: `'https://imager.bbxnet.sk/timelapse_dolna_ulica/'yyyyMMdd/HHmm/ss'.jpg'`,
        step: 10,
      }
    })
  // the current playback state.
  const [state, setState] = useSetState(
    function () {
      return {
        current: setMilliseconds(setSeconds(new Date(), 0), 0),
        // the start clip datetime.
        start: null,
        // the stop clip datetime.
        stop: null,
      }
    })
  // the latest state value.
  const stateLatest = useLatest(state)

  const [open, setOpen] = useBoolState(false)

  const [timeout, setTimeout] = useState(null)

  // the play-pause callback.
  const playPauseCallback = useCallback(
    function () {
      setTimeout(
        function (state) {
          return state === null ? option.interval : null
        })
    }, [option, setTimeout])

  // update state callback.
  const updateStateCallback = useCallback(
    function (event) {
      const target = event.currentTarget
      setState(
        function (state) {
          return {[target.name]: state.current}
        },
      )
    }, [setState])

  const prev = useCallback(
    function () {
      setState(
        function (state) {
          return {
            current: subSeconds(state.current, option.step),
          }
        })
    }, [option, setState])

  const next = useCallback(
    function () {
      setState(
        function (state) {
          return {
            current: addSeconds(state.current, option.step),
          }
        })
    }, [option, setState])

  // update state datetime value callback.
  const updateDateTimeCallback = useCallback(
    function (event) {
      if (event.target.name && event.target.value && event.target.type === 'date') {
        setState(
          function (state) {
            const split = event.target.value.split('-')
            return {
              [event.target.name]: set(state[event.target.name] || state.current, {year: parseInt(split[0]), month: parseInt(split[1]) - 1, date: parseInt(split[2])}),
            }
          })
      } else if (event.target.name && event.target.value && event.target.type === 'time') {
        setState(
          function (state) {
            const split = event.target.value.split(':')
            return {
              [event.target.name]: set(state[event.target.name] || state.current, {hours: parseInt(split[0]), minutes: parseInt(split[1]), seconds: parseInt(split[2])}),
            }
          })
      }
    }, [setState])

  // the open settings callback.
  const openSettingsCallback = useCallback(
    function () {
      setTimeout(null)
      setOpen(true)
    }, [])

  // update an option value callback.
  const updateOptionCallback = useCallback(
    function (event) {
      if (event.target.name && event.target.value) {
        setOption({[event.target.name]: event.target.value})
      }
    }, [setOption])

  // play the timelapse using `setInterval`.
  useInterval(useCallback(
    function () {
      if (isEqual(stateLatest.current.current, stateLatest.current.stop)) {
        setTimeout(null)
      } else {
        next()
      }
    }, [next, stateLatest]), timeout)

  return (
    <View className="flex min-h-screen flex-col bg-neutral-50 text-neutral-700">
      <Panel styled="boxed" className="py-3 px-4">
        <Heading as="h1" className="!font-bold">
          timelapsed - timelapse made easy!
        </Heading>
      </Panel>

      <Main className="grow">
        <Panel styled="boxed" className="py-3 px-4">
          <View className="relative flex justify-center">
            <img alt="" src={state.current ? format(state.current, option.location) : ''}/>
          </View>

          <View className="flex py-3 gap-2 justify-between items-center">
            <View className="flex basis-1/3 justify-start items-stretch">
              <input name="start" type="date" value={state.start ? format(state.start, 'yyyy-MM-dd') : ''} onChange={updateDateTimeCallback} className="py-2 px-2 pl-4 border-y border-l rounded-l-full"/>
              <input name="start" step="10" type="time" value={state.start ? format(state.start, 'HH:mm:ss') : ''} onChange={updateDateTimeCallback} className="py-2 px-2 border-y"/>

              <Button name="start" onClick={updateStateCallback} styled="teal-500" className="py-2 pr-2 border-y border-r rounded-r-full">
                <MDI name="mdi mdi-chevron-double-left" className="py-1 px-2 pl-3 text-xl border-l"/>
              </Button>
            </View>

            <View className="flex basis-1/3 justify-center">
              <input
                name="current"
                type="date"
                value={state.current ? format(state.current, 'yyyy-MM-dd') : ''}
                onChange={updateDateTimeCallback}
                className="py-2 px-2 pl-4 border-y border-l rounded-l-full"
              />

              <input
                name="current"
                step="10"
                type="time"
                value={state.current ? format(state.current, 'HH:mm:ss') : ''}
                onChange={updateDateTimeCallback}
                className="py-2 px-2 pr-4 border-y border-r rounded-r-full"
              />
            </View>

            <View className="flex basis-1/3 justify-end items-stretch">
              <Button name="stop" onClick={updateStateCallback} styled="teal-500" className="py-2 pl-2 border-y border-l rounded-l-full">
                <MDI name="mdi mdi-chevron-double-right" className="py-1 px-2 pr-3 text-xl border-r"/>
              </Button>

              <input name="stop" type="date" value={state.stop ? format(state.stop, 'yyyy-MM-dd') : ''} onChange={updateDateTimeCallback} className="py-2 px-2 border-y"/>
              <input name="stop" step="10" type="time" value={state.stop ? format(state.stop, 'HH:mm:ss') : ''} onChange={updateDateTimeCallback} className="py-2 px-2 pr-2 border-y border-r rounded-r-full"/>
            </View>
          </View>

          <View className="flex justify-center">
            <View className="flex basis-1/3 justify-start">
            </View>

            <View className="flex basis-1/3 justify-center">
              <Button disabled={state.start === null} onClick={() => setState({current: state.start})} styled="teal-500" className="py-2 px-4 border-y border-l rounded-l-full">
                <MDI name="mdi mdi-skip-backward" className="text-xl"/>
              </Button>

              <Button onClick={prev} styled="teal-500" className="py-2 px-4 border-y">
                <MDI name="mdi mdi-skip-previous" className="text-xl"/>
              </Button>

              <Button onClick={playPauseCallback} styled="teal-500" className="py-2 px-4 border-y">
                <MDI name={cc(['mdi', {'mdi-play': timeout === null, 'mdi-pause': timeout !== null}])} className="text-xl"/>
              </Button>

              <Button onClick={next} styled="teal-500" className="py-2 px-4 border-y">
                <MDI name="mdi mdi-skip-next" className="text-xl"/>
              </Button>

              <Button disabled={state.stop === null} onClick={() => setState({current: state.stop})} styled="teal-500" className="py-2 px-4 border-y border-r rounded-r-full">
                <MDI name="mdi mdi-skip-forward" className="text-xl"/>
              </Button>
            </View>

            <View className="flex gap-2 basis-1/3 justify-end items-center">
              <Button as="a" href={state.current ? format(state.current, option.location) : ''} download styled="teal-500" className="py-2 px-6 border rounded-full">
                <MDI name="mdi mdi-download" className="mr-2 -ml-2"/> Download
              </Button>

              <Button disabled={true} styled="teal-500" className="py-2 px-6 border rounded-full">
                <MDI name="mdi mdi-video" className="mr-2 -ml-2"/> To Video
              </Button>
            </View>
          </View>
        </Panel>
      </Main>

      <Backdrop hidden={!open} styled="black/50" onClick={setOpen}/>

      <View as="aside" hidden={!open} className="absolute w-96 bg-neutral-100 shadow-md inset-y-0 right-0">
        <View className="flex py-2 px-3 gap-2 items-center border-b">
          <MDI name="mdi mdi-cog"/>

          <Heading as="h3" className="uppercase">
            Settings
          </Heading>

          <Button onClick={setOpen} className="py-2 px-2 ml-auto hover:text-teal-500 leading-none hover:bg-neutral-200 rounded-full">
            <MDI name="mdi mdi-close" className="text-xl leading-none"/>
          </Button>
        </View>

        <View className="flex py-2 px-3 gap-2 flex-col">
          <TextField
            label="Location"
            name="location"
            value={option.location}
            onChange={updateOptionCallback}
          />

          <TextField
            label="step"
            name="step"
            type="number"
            value={option.step}
            min="10"
            step="10"
            onChange={updateOptionCallback}
          />

          <TextField
            label="Framerate"
            name="framerate"
            type="number"
            value={option.framerate}
            min="1"
            max="50"
            onChange={updateOptionCallback}
          />

          <TextField
            label="Interval"
            name="interval"
            type="number"
            value={option.interval}
            min="50"
            step="10"
            onChange={updateOptionCallback}
          />
        </View>
      </View>

      <View as="aside" hidden={open} className="absolute top-2/3 right-0">
        <Button onClick={openSettingsCallback} className="py-4 px-4 text-xl hover:text-teal-500 shadow-md border rounded-l-lg">
          <MDI name="mdi mdi-cog"/>
        </Button>
      </View>

      <Panel styled="boxed" className="py-2 px-3">
        <View className="text-sm">
          © 2023 Tomáš Havlas. Powered by timelapsed.
        </View>
      </Panel>
    </View>
  )
}

export default ROUTE__index
