import {useNanoId} from '@havlasme/react-toolkit'
import cc from 'classcat'
import View from '../View'

const TextField = function ({id, label, className, containerClassName, labelClassName, ...rest}) {
  const htmlId = useNanoId(id)

  return (
    <View className={containerClassName}>
      <View as="label" htmlFor={htmlId} className={cc(['block mb-1 font-medium uppercase', labelClassName])}>
        {label}
      </View>

      <View as="input" id={htmlId} className={cc(['py-2 px-3 w-full border', className])} {...rest}/>
    </View>
  )
}

export default TextField
