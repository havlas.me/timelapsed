import cc from 'classcat'
import View from '../View'

const Heading = function ({children, className, ...rest}) {
  return (
    <View as="h2" className={cc(['font-medium', className])} {...rest}>
      {children}
    </View>
  )
}

export default Heading
