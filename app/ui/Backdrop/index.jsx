import cc from 'classcat'
import View from '../View'

const usesStyled = function (styled) {
  return {
    'bg-black/50': styled === 'black/50',
  }
}

const Backdrop = function ({children, styled, className, ...rest}) {
  return (
    <View className={cc(['fixed inset-0', usesStyled(styled), className])} {...rest}>
      {children}
    </View>
  )
}

export default Backdrop
