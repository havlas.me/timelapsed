const View = function ({as: Component = 'div', children, ...rest}) {
  return (
    <Component {...rest}>
      {children}
    </Component>
  )
}

export default View
