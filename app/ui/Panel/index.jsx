import cc from 'classcat'
import View from '../View'

const usesStyled = function (styled) {
  return {
    'mx-auto container': styled === 'boxed',
  }
}

const Panel = function ({children, styled, className, containerClassName, ...rest}) {
  return (
    <View className={containerClassName}>
      <View className={cc([usesStyled(styled), className])} {...rest}>
        {children}
      </View>
    </View>
  )
}

export default Panel
