import cc from 'classcat'
import View from '../View'

const usesStyled = function (styled) {
  return {
    'text-neutral-700 disabled:text-neutral-400 enabled:focus-visible:text-teal-500 enabled:hover:text-teal-500 bg-white enabled:focus-visible:bg-neutral-200 enabled:hover:bg-neutral-200': styled === 'teal-500',
  }
}

const Button = function ({children, styled, className, ...rest}) {
  return (
    <View as="button" type="button" className={cc(['transition', usesStyled(styled), className])} {...rest}>
      {children}
    </View>
  )
}

export default Button
