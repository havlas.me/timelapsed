import cc from 'classcat'
import View from '../View'

const MDI = function ({name, className, ...rest}) {
  return (
    <View as="span" className={cc([name, className])} {...rest}/>
  )
}

export default MDI
