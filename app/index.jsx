import React from 'react'
import ReactDOM from 'react-dom/client'
import ROUTE__index from './route'
import './tailwind.css'

ReactDOM.createRoot(
  document.getElementById('root'),
).render(
  <React.StrictMode>
    <ROUTE__index/>
  </React.StrictMode>,
)
